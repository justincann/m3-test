import $ from 'jquery';
import 'what-input';

// Foundation JS relies on a global varaible. In ES6, all imports are hoisted
// to the top of the file so if we used`import` to import Foundation,
// it would execute earlier than we have assigned the global variable.
// This is why we have to use CommonJS require() here since it doesn't
// have the hoisting behavior.
window.jQuery = $;
require('foundation-sites');

// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
//import './lib/foundation-explicit-pieces';


$(document).foundation();

// Menu
//----------------------------------------------------------------------------
(function ($) {

    $(function () {

        'use strict';

        // constants
        //-------------------------------------------

        const SUBMENU_ITEM_CLASS = "submenu-item";

        // ELements
        //-------------------------------------------

        const section = $('section');
        const searchJobs = $('.search-jobs');
        const searchButton = $('#search-button');

        const secondaryMenuItem = $('.secondary a');
        const linksList = $('.links .links-list');
        const linksTitle = $('.links .links-title');

        const menuIcon = $("body .menu-icon");
        const ctaMenu = $("body #cta-menu");
        console.debug(menuIcon);

        // normally i'd use an enum here
        const menuDictionary = {
            "candidates": [
                "Nursing",
                "Psychological Therapies",
                "Allied Health",
                "Executive",
                "Social Work",
                "Health Sciences",
                "Psychiatry Doctors",
                "Acute Doctors",
                "Child Care",
            ],
            "clients": [
                "Clinical governance",
            ],
            "about-us": [
                "Careers with us",
                "Charity work",
                "Media",
                "Awards and accreditation",
            ]
        }

        // Methods
        //-------------------------------------------

        /**
         * Displays links from menu item
         *
         * @param {object} event
         */
        function displayLinks(event) {
            const menuElement = $(event.target);
            const menu = event.target.id;

            const submenuItems = menuDictionary[menu];

            const newSubmenuItem = (item) =>
                `<div class="cell ${SUBMENU_ITEM_CLASS}">
                    <a href="${item.toLowerCase().replace(" ", "-")}">
                        ${item}
                    </a>
                </div>`;

            $(linksTitle).html(menu.replace("-", " "));

            // clear list
            $(`.${SUBMENU_ITEM_CLASS}`).remove();

            console.debug(menuElement.text())

            submenuItems.forEach((item) => {
                // place item into list
                $(linksList).append(newSubmenuItem(item));
                $(menuElement).after(newSubmenuItem(item));
            });
        }

        // Events
        //-------------------------------------------

        if (
            searchJobs.length > 0 &&
            searchButton.length > 0
        ) {

            searchButton.click(() => {
                searchJobs.toggle();
            });

            section.click(() => {
                searchJobs.hide();
            });

        };

        if (
            linksList.length > 0 &&
            linksTitle.length > 0
        ) {

            secondaryMenuItem.click(displayLinks);

            section.click(() => {
                $(`.${SUBMENU_ITEM_CLASS}`).remove();
                $(linksTitle).html("");
            });

        };

        if (
            menuIcon.length > 0 &&
            ctaMenu.length > 0
        ) {
            section.click(() => {
                if ($(ctaMenu).is(":visible")) {
                    $(menuIcon).trigger('click');
                }
            });
        }

    });

})(jQuery);
